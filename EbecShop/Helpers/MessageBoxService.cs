﻿using System.Windows;

namespace EbecShop.Helpers
{
    internal interface IMessageBoxService
    {
        void ShowNotification(string text, string caption);
        bool AskForConfirmation(string text, string caption);
    }

    public class MessageBoxService : IMessageBoxService
    {
        public void ShowNotification(string text, string caption)
        {
            MessageBox.Show(text, caption);
        }

        public bool AskForConfirmation(string text, string caption)
        {
            return MessageBox.Show(text, caption, MessageBoxButton.YesNo, MessageBoxImage.Question) ==
                   MessageBoxResult.Yes;
        }
    }
}