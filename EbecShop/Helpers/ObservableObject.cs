﻿using System.ComponentModel;

namespace EbecShop.Helpers
{
    public class ObservableObject : INotifyPropertyChanged
    {
        protected MessageBoxService mBoxService = new MessageBoxService();
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}