﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using EbecShop.Helpers;
using EbecShop.Serializers;

namespace EbecShop.ViewModels
{
    public class MembersViewModel : ObservableObject, IDataErrorInfo
    {
        #region Constructors

        public MembersViewModel()
        {
            Members = MembersSerializer.DeserializationToViewModel();
            Teams = TeamsSerializer.DeserializationToViewModel();
            SetTeamsNames();
            CurrentMember = new MemberViewModel();
            SelectedMember = new MemberViewModel();
            Team = new TeamViewModel();
            EditMode = false;
            ascOrderColumn = "Id";
        }

        #endregion Constructors

        string IDataErrorInfo.Error
        {
            get { return String.Empty; }
        }

        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                string result = null;
                if (columnName == "FirstName")
                {
                    if (string.IsNullOrEmpty(CurrentMember.FirstName))
                        result = "Imię nie może być puste!";
                }
                return result;
            }
        }

        #region Members

        private MemberViewModel _currentMember;
        private MemberViewModel _selectedMember;
        private ObservableCollection<TeamViewModel> _teams;
        private TeamViewModel _team;
        private bool _editMode;
        private string ascOrderColumn;

        #endregion Members

        #region Properties

        public ObservableCollection<MemberViewModel> Members { get; set; }

        public MemberViewModel CurrentMember
        {
            get { return _currentMember; }
            set
            {
                _currentMember = value;
                RaisePropertyChanged("CurrentMember");
            }
        }

        public MemberViewModel SelectedMember
        {
            get { return _selectedMember; }
            set
            {
                _selectedMember = value;
                RaisePropertyChanged("SelectedMember");
            }
        }

        public ObservableCollection<TeamViewModel> Teams
        {
            get { return _teams; }
            set
            {
                _teams = value;
                RaisePropertyChanged("Teams");
            }
        }

        public TeamViewModel Team
        {
            get { return _team; }
            set
            {
                _team = value;
                if (_team != null) CurrentMember.TeamId = _team.Id;
                RaisePropertyChanged("Team");
            }
        }

        public bool EditMode
        {
            get { return _editMode; }
            set
            {
                _editMode = value;
                RaisePropertyChanged("EditMode");
            }
        }

        #endregion Properties

        #region Commands

        #region EditMemberCommand

        private RelayCommand _editMemberCommand;

        public RelayCommand EditMemberCommand
        {
            get
            {
                return _editMemberCommand ?? (_editMemberCommand = new RelayCommand
                    (
                    param => Edit(),
                    param => CanEdit()
                    ));
            }
            set { _editMemberCommand = value; }
        }

        private void Edit()
        {
            CurrentMember = new MemberViewModel(SelectedMember);
            Team = Teams.First(t => t.Id == SelectedMember.TeamId);
            EditMode = true;
        }

        private bool CanEdit()
        {
            return !EditMode &&
                   SelectedMember != null &&
                   SelectedMember.Id != 0;
        }

        #endregion EditMemberCommand

        #region SaveMemberEditCommand

        private RelayCommand _saveMemberEditCommand;

        public RelayCommand SaveMemberEditCommand
        {
            get
            {
                return _saveMemberEditCommand ?? (_saveMemberEditCommand = new RelayCommand
                    (
                    param => SaveEdit(),
                    param => CanSaveEdit()
                    ));
            }
            set { _saveMemberEditCommand = value; }
        }

        private void SaveEdit()
        {
            SelectedMember = Members.First(m => m.Id == CurrentMember.Id);

            SelectedMember.FirstName = CurrentMember.FirstName;
            SelectedMember.LastName = CurrentMember.LastName;
            SelectedMember.TeamId = CurrentMember.TeamId;

            CurrentMember = new MemberViewModel();
            EditMode = false;
            Save();
            RefreshData();
        }

        private bool CanSaveEdit()
        {
            return MemberIsValid(CurrentMember) &&
                   MemberWithThisIdAlreadyExists(CurrentMember.Id) &&
                   EditMode;
        }

        #endregion SaveMemberEditCommand

        #region CancelMemberEditCommand

        private RelayCommand _cancelMemberEditCommand;

        public RelayCommand CancelMemberEditCommand
        {
            get
            {
                return _cancelMemberEditCommand ?? (_cancelMemberEditCommand = new RelayCommand
                    (
                    param => CancelEdit(),
                    param => CanCancelEdit()
                    ));
            }
            set { _cancelMemberEditCommand = value; }
        }

        private void CancelEdit()
        {
            CurrentMember = new MemberViewModel();
            EditMode = false;
        }

        private bool CanCancelEdit()
        {
            return EditMode;
        }

        #endregion SaveMemberEditCommand

        #region AddMemberCommand

        private RelayCommand _addMemberCommand;

        public RelayCommand AddMemberCommand
        {
            get
            {
                return _addMemberCommand ?? (_addMemberCommand = new RelayCommand
                    (
                    param => AddMember(),
                    param => CanAddMember()
                    ));
            }
            set { _addMemberCommand = value; }
        }

        private void AddMember()
        {
            Members.Add(CurrentMember);
            Save();
            RefreshData();
            SelectedMember = Members.First(m => m.Id == CurrentMember.Id);
            CurrentMember = new MemberViewModel();
        }

        private bool CanAddMember()
        {
            return MemberIsValid(CurrentMember) &&
                   !MemberWithThisIdAlreadyExists(CurrentMember.Id) &&
                   !EditMode;
        }

        #endregion AddMemberCommand

        #region DeleteMemberCommand

        private RelayCommand _deleteMemberCommand;

        public RelayCommand DeleteMemberCommand
        {
            get
            {
                return _deleteMemberCommand ?? (_deleteMemberCommand = new RelayCommand
                    (
                    param => DeleteMember(),
                    param => CanDeleteMember()
                    ));
            }
            set { _deleteMemberCommand = value; }
        }

        private void DeleteMember()
        {
            Members.Remove(SelectedMember);
            Save();
            SelectedMember = new MemberViewModel();
        }

        private bool CanDeleteMember()
        {
            return MemberAlreadyExists(SelectedMember) &&
                   !EditMode;
        }

        #endregion DeleteMemberCommand

        #region ClearFormCommand

        private RelayCommand _clearFormCommand;

        public RelayCommand ClearFormCommand
        {
            get
            {
                return _clearFormCommand ?? (_clearFormCommand = new RelayCommand
                    (
                    param => ClearForm(),
                    param => CanClearForm()
                    ));
            }
            set { _clearFormCommand = value; }
        }

        private void ClearForm()
        {
            CurrentMember = new MemberViewModel();
        }

        private bool CanClearForm()
        {
            return !EditMode;
        }

        #endregion ClearFormCommand

        #region RemoveAllMembersCommand

        private RelayCommand _removeAllMembersCommand;

        public RelayCommand RemoveAllMembersCommand
        {
            get
            {
                return _removeAllMembersCommand ?? (_removeAllMembersCommand = new RelayCommand
                    (
                    param => RemoveAllMembers(),
                    param => CanRemoveAllMembers()
                    ));
            }
            set { _removeAllMembersCommand = value; }
        }

        private void RemoveAllMembers()
        {
            if (mBoxService.AskForConfirmation("Do you want to remove all members?", "Confirmation"))
            {
                Members.Clear();
                MembersSerializer.SerializationFromViewModel(Members);
                CurrentMember = new MemberViewModel();
                SelectedMember = new MemberViewModel();
                EditMode = false;
            }
        }

        private bool CanRemoveAllMembers()
        {
            return Members.Any() && !EditMode;
        }

        #endregion RemoveAllMembersCommand

        #region RefreshDataCommand

        private RelayCommand _refreshDataCommand;

        public RelayCommand RefreshDataCommand
        {
            get
            {
                if (_refreshDataCommand == null)
                {
                    _refreshDataCommand = new RelayCommand
                        (
                        param => RefreshData(),
                        param => CanRefreshData()
                        );
                }
                return _refreshDataCommand;
            }

            set { _refreshDataCommand = value; }
        }


        private bool CanRefreshData()
        {
            return true;
        }

        #endregion RefreshDataCommand

        #region SortMemberCommand

        private RelayCommand _sortMemberCommand;

        public RelayCommand SortMemberCommand
        {
            get
            {
                return _sortMemberCommand ?? (_sortMemberCommand = new RelayCommand
                    (
                    Sort,
                    param => CanSort()
                    ));
            }
            set { _sortMemberCommand = value; }
        }

        private void Sort(object parametr)
        {
            if (!(parametr is string))
                return;
            var memberId = SelectedMember != null ? SelectedMember.Id : -1;
            var column = (string) parametr;

            if (column != ascOrderColumn)
            {
                SortAsc(SelectSelector(column));
                ascOrderColumn = column;
            }
            else
            {
                SortDesc(SelectSelector(column));
                ascOrderColumn = "";
            }

            Save();
            if (Members.Any(i => i.Id == memberId))
                SelectedMember = Members.First(i => i.Id == memberId);

            RaisePropertyChanged("Members");
        }

        private Func<MemberViewModel, object> SelectSelector(string columnName)
        {
            switch (columnName)
            {
                case "Id":
                    return (i => i.Id);
                case "FirstName":
                    return (i => i.FirstName);
                case "LastName":
                    return (i => i.LastName);
                case "TeamId":
                    return (i => i.TeamId);
                case "TeamName":
                    return (i => i.TeamName);
                default:
                    return (i => i.Id);
            }
        }

        public void SortAsc(Func<MemberViewModel, object> selector)
        {
            Members = new ObservableCollection<MemberViewModel>(Members.OrderBy(selector));
        }

        public void SortDesc(Func<MemberViewModel, object> selector)
        {
            Members = new ObservableCollection<MemberViewModel>(Members.OrderByDescending(selector));
        }

        private bool CanSort()
        {
            return true;
        }

        #endregion SortMemberCommand

        #endregion Commands

        #region Methods

        public void Save()
        {
            MembersSerializer.SerializationFromViewModel(Members);
        }

        public void RefreshData()
        {
            RefreshMembers();
            RefreshTeams();
            SetTeamsNames();
        }

        private void RefreshMembers()
        {
            try
            {
                var memberId = SelectedMember.Id;
                Members = MembersSerializer.DeserializationToViewModel();
                Members = new ObservableCollection<MemberViewModel>(Members.OrderBy(i => i.Id));
                if (Members.Any(m => m.Id == memberId))
                    SelectedMember = Members.First(m => m.Id == memberId);
            }
            catch (Exception)
            {
                // ignored
            }
            RaisePropertyChanged("Members");
        }

        private void RefreshTeams()
        {
            try
            {
                Teams = TeamsSerializer.DeserializationToViewModel();
            }
            catch (Exception)
            {
                // ignored
            }
            RaisePropertyChanged("Teams");
        }

        private bool MemberIsValid(MemberViewModel member)
        {
            return member.Id > 0 &&
                   !String.IsNullOrWhiteSpace(member.FirstName) &&
                   !String.IsNullOrWhiteSpace(member.LastName) &&
                   member.TeamId > 0 &&
                   MemberWithThisIdIsNotFull(member.TeamId) &&
                   ExistsTeamWithThisId(member.TeamId);
        }

        private bool MemberWithThisIdAlreadyExists(int id)
        {
            return Members.Any(member => member.Id == id);
        }

        private bool MemberAlreadyExists(MemberViewModel member)
        {
            return Members.Any(m => m.Equals(member));
        }

        private bool ExistsTeamWithThisId(int teamId)
        {
            return Teams.Any(t => t.Id == teamId);
        }

        private bool MemberWithThisIdIsNotFull(int teamId)
        {
            return Members.Count(m => m.TeamId == teamId) < 4;
        }

        private void SetTeamsNames()
        {
            foreach (var member in Members.Where(m => Teams.Any(t => m.TeamId == t.Id)))
            {
                member.TeamName = Teams.First(t => t.Id == member.TeamId).Name;
            }
        }

        #endregion Methods
    }
}