﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using EbecShop.Helpers;
using EbecShop.Serializers;

namespace EbecShop.ViewModels
{
    public class TransactionsViewModel : ObservableObject
    {
        #region Constructors

        public TransactionsViewModel()
        {
            Transactions = TransactionsSerializer.DeserializationToViewModel();
            Items = ItemsSerializer.DeserializationToViewModel();
            Teams = TeamsSerializer.DeserializationToViewModel();
            SetItemsNames();
            SetTeamsNames();
            CurrentTransaction = new TransactionViewModel();
            SelectedTransaction = new TransactionViewModel();
            EditMode = false;
            _ascOrderColumn = "Id";
        }

        #endregion Constructors

        #region Members

        private TransactionViewModel _currentTransaction;
        private TransactionViewModel _selectedTransaction;
        private ObservableCollection<ItemViewModel> _items;
        private ObservableCollection<TeamViewModel> _teams;
        private bool _editMode;
        private string _ascOrderColumn;

        #endregion Members

        #region Properties

        public ObservableCollection<TransactionViewModel> Transactions { get; set; }

        public TransactionViewModel CurrentTransaction
        {
            get { return _currentTransaction; }
            set
            {
                _currentTransaction = value;
                RaisePropertyChanged("CurrentTransaction");
            }
        }

        public TransactionViewModel SelectedTransaction
        {
            get { return _selectedTransaction; }
            set
            {
                _selectedTransaction = value;
                RaisePropertyChanged("SelectedTransaction");
            }
        }

        public ObservableCollection<ItemViewModel> Items
        {
            get { return _items; }
            set
            {
                _items = value;
                RaisePropertyChanged("Items");
            }
        }

        public ObservableCollection<TeamViewModel> Teams
        {
            get { return _teams; }
            set
            {
                _teams = value;
                RaisePropertyChanged("Teams");
            }
        }

        public bool EditMode
        {
            get { return _editMode; }
            set
            {
                _editMode = value;
                RaisePropertyChanged("EditMode");
            }
        }

        #endregion Properties

        #region Commands

        #region EditTransactionCommand

        private RelayCommand _editTransactionCommand;

        public RelayCommand EditTransactionCommand
        {
            get
            {
                return _editTransactionCommand ?? (_editTransactionCommand = new RelayCommand
                    (
                    param => Edit(),
                    param => CanEdit()
                    ));
            }
            set { _editTransactionCommand = value; }
        }

        private void Edit()
        {
            CurrentTransaction = new TransactionViewModel(SelectedTransaction);
            EditMode = true;
        }

        private bool CanEdit()
        {
            return !EditMode &&
                   SelectedTransaction != null &&
                   SelectedTransaction.Id != 0;
        }

        #endregion EditTransactionCommand

        #region SaveTransactionEditCommand

        private RelayCommand _saveTransactionEditCommand;

        public RelayCommand SaveTransactionEditCommand
        {
            get
            {
                return _saveTransactionEditCommand ?? (_saveTransactionEditCommand = new RelayCommand
                    (
                    param => SaveEdit(),
                    param => CanSaveEdit()
                    ));
            }
            set { _saveTransactionEditCommand = value; }
        }

        private void SaveEdit()
        {
            SelectedTransaction = Transactions.First(m => m.Id == CurrentTransaction.Id);

            SelectedTransaction.Quantity = CurrentTransaction.Quantity;
            SelectedTransaction.Price = CurrentTransaction.Price;
            SelectedTransaction.TeamId = CurrentTransaction.TeamId;
            SelectedTransaction.ItemId = CurrentTransaction.ItemId;
            SelectedTransaction.Data = CurrentTransaction.Data;

            CurrentTransaction = new TransactionViewModel();
            EditMode = false;
            Save();
            RefreshData();
        }

        private bool CanSaveEdit()
        {
            return TransactionIsValid(CurrentTransaction) &&
                   TransactionWithThisIdAlreadyExists(CurrentTransaction.Id) &&
                   EditMode;
        }

        #endregion SaveTransactionEditCommand

        #region CancelTransactionEditCommand

        private RelayCommand _cancelTransactionEditCommand;

        public RelayCommand CancelTransactionEditCommand
        {
            get
            {
                return _cancelTransactionEditCommand ?? (_cancelTransactionEditCommand = new RelayCommand
                    (
                    param => CancelEdit(),
                    param => CanCancelEdit()
                    ));
            }
            set { _cancelTransactionEditCommand = value; }
        }

        private void CancelEdit()
        {
            CurrentTransaction = new TransactionViewModel();
            EditMode = false;
        }

        private bool CanCancelEdit()
        {
            return EditMode;
        }

        #endregion SaveTransactionEditCommand

        #region AddTransactionCommand

        private RelayCommand _addTransactionCommand;

        public RelayCommand AddTransactionCommand
        {
            get
            {
                return _addTransactionCommand ?? (_addTransactionCommand = new RelayCommand
                    (
                    param => AddTransaction(),
                    param => CanAddTransaction()
                    ));
            }
            set { _addTransactionCommand = value; }
        }

        private void AddTransaction()
        {
            Transactions.Add(CurrentTransaction);
            Save();
            RefreshData();
            SelectedTransaction = Transactions.First(t => t.Id == CurrentTransaction.Id);
            CurrentTransaction = new TransactionViewModel();
        }

        private bool CanAddTransaction()
        {
            return TransactionIsValid(CurrentTransaction) &&
                   !TransactionWithThisIdAlreadyExists(CurrentTransaction.Id) &&
                   !EditMode;
        }

        #endregion AddTransactionCommand

        #region DeleteTransactionCommand

        private RelayCommand _deleteTransactionCommand;

        public RelayCommand DeleteTransactionCommand
        {
            get
            {
                return _deleteTransactionCommand ?? (_deleteTransactionCommand = new RelayCommand
                    (
                    param => DeleteTransaction(),
                    param => CanDeleteTransaction()
                    ));
            }
            set { _deleteTransactionCommand = value; }
        }

        private void DeleteTransaction()
        {
            Transactions.Remove(SelectedTransaction);
            Save();
            SelectedTransaction = new TransactionViewModel();
        }

        private bool CanDeleteTransaction()
        {
            return TransactionAlreadyExists(SelectedTransaction) &&
                   !EditMode;
        }

        #endregion DeleteTransactionCommand

        #region ClearFormCommand

        private RelayCommand _clearFormCommand;

        public RelayCommand ClearFormCommand
        {
            get
            {
                return _clearFormCommand ?? (_clearFormCommand = new RelayCommand
                    (
                    param => ClearForm(),
                    param => CanClearForm()
                    ));
            }
            set { _clearFormCommand = value; }
        }

        private void ClearForm()
        {
            CurrentTransaction = new TransactionViewModel();
        }

        private bool CanClearForm()
        {
            return !EditMode;
        }

        #endregion ClearFormCommand

        #region RemoveAllTransactionsCommand

        private RelayCommand _removeAllTransactionsCommand;

        public RelayCommand RemoveAllTransactionsCommand
        {
            get
            {
                return _removeAllTransactionsCommand ?? (_removeAllTransactionsCommand = new RelayCommand
                    (
                    param => RemoveAllTransactions(),
                    param => CanRemoveAllTransactions()
                    ));
            }
            set { _removeAllTransactionsCommand = value; }
        }

        private void RemoveAllTransactions()
        {
            if (mBoxService.AskForConfirmation("Do you want to remove all transactions?", "Confirmation"))
            {
                Transactions.Clear();
                Save();
                CurrentTransaction = new TransactionViewModel();
                SelectedTransaction = new TransactionViewModel();
                EditMode = false;
            }
        }

        private bool CanRemoveAllTransactions()
        {
            return Transactions.Any() &&
                   !EditMode;
        }

        #endregion RemoveAllTransactionsCommand

        #region RefreshDataCommand

        private RelayCommand _refreshDataCommand;

        public RelayCommand RefreshDataCommand
        {
            get
            {
                return _refreshDataCommand ?? (_refreshDataCommand = new RelayCommand
                    (
                    param => RefreshData(),
                    param => CanRefreshData()
                    ));
            }

            set { _refreshDataCommand = value; }
        }

        private bool CanRefreshData()
        {
            return true;
        }

        #endregion RefreshDataCommand

        #region SortTransactionCommand

        private RelayCommand _sortTransactionCommand;

        public RelayCommand SortTransactionCommand
        {
            get
            {
                return _sortTransactionCommand ?? (_sortTransactionCommand = new RelayCommand
                    (
                    Sort,
                    param => CanSort()
                    ));
            }
            set { _sortTransactionCommand = value; }
        }

        private void Sort(object parametr)
        {
            if (!(parametr is string))
                return;

            var transactionId = SelectedTransaction != null ? SelectedTransaction.Id : -1;
            var column = (string) parametr;

            if (column != _ascOrderColumn)
            {
                SortAsc(SelectSelector(column));
                _ascOrderColumn = column;
            }
            else
            {
                SortDesc(SelectSelector(column));
                _ascOrderColumn = "";
            }

            Save();
            if (Transactions.Any(i => i.Id == transactionId))
                SelectedTransaction = Transactions.First(i => i.Id == transactionId);

            RaisePropertyChanged("Transactions");
        }

        private Func<TransactionViewModel, object> SelectSelector(string columnName)
        {
            switch (columnName)
            {
                case "Id":
                    return (i => i.Id);
                case "TeamId":
                    return (i => i.TeamId);
                case "TeamName":
                    return (i => i.TeamName);
                case "ItemId":
                    return (i => i.ItemId);
                case "ItemName":
                    return (i => i.ItemName);
                case "Quantity":
                    return (i => i.Quantity);
                case "Price":
                    return (i => i.Price);
                case "Data":
                    return (i => i.Data);
                default:
                    return (i => i.Id);
            }
        }

        public void SortAsc(Func<TransactionViewModel, object> selector)
        {
            Transactions = new ObservableCollection<TransactionViewModel>(Transactions.OrderBy(selector));
        }

        public void SortDesc(Func<TransactionViewModel, object> selector)
        {
            Transactions = new ObservableCollection<TransactionViewModel>(Transactions.OrderByDescending(selector));
        }

        private bool CanSort()
        {
            return true;
        }

        #endregion SortTransactionCommand

        #endregion Commands

        #region Methods

        public void Save()
        {
            TransactionsSerializer.SerializationFromViewModel(Transactions);
        }

        public void RefreshData()
        {
            RefreshItems();
            RefreshTeams();
            RefreshTransactions();
            SetItemsNames();
            SetTeamsNames();
        }

        private void RefreshItems()
        {
            try
            {
                Items = ItemsSerializer.DeserializationToViewModel();
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private void RefreshTeams()
        {
            try
            {
                Teams = TeamsSerializer.DeserializationToViewModel();
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private void RefreshTransactions()
        {
            try
            {
                //TODO SORTING AFTER READ
                //int transactionId = SelectedTransaction.Id;
                Transactions = TransactionsSerializer.DeserializationToViewModel();
                //Transactions = new ObservableCollection<TransactionViewModel>(Transactions.OrderBy(i => i.Id));
                //lectedTransaction = Transactions.First(t => t.Id == transactionId);
            }
            catch (Exception)
            {
                // ignored
            }
            RaisePropertyChanged("Transactions");
        }

        private bool TransactionIsValid(TransactionViewModel trasaction)
        {
            return trasaction.Id > 0 &&
                   trasaction.Quantity > 0 &&
                   trasaction.TeamId > 0 &&
                   trasaction.ItemId > 0;
        }

        private bool TransactionWithThisIdAlreadyExists(int id)
        {
            return Transactions.Any(item => item.Id == id);
        }

        private bool TransactionAlreadyExists(TransactionViewModel transaction)
        {
            return Transactions.Any(t => t.Equals(transaction));
        }

        private void SetItemsNames()
        {
            foreach (var transaction in Transactions.Where(t => Items.Any(i => i.Id == t.ItemId)))
            {
                transaction.ItemName = Items.First(i => i.Id == transaction.ItemId).Name;
            }
        }

        private void SetTeamsNames()
        {
            foreach (var transaction in Transactions.Where(t => Teams.Any(n => n.Id == t.TeamId)))
            {
                transaction.TeamName = Teams.First(t => t.Id == transaction.TeamId).Name;
            }
        }

        #endregion Methods
    }
}