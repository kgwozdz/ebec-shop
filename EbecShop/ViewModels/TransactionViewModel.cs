﻿using System;
using EbecShop.Helpers;
using EbecShop.Models;

namespace EbecShop.ViewModels
{
    public class TransactionViewModel : ObservableObject
    {

        public TransactionViewModel()
        {
            Transaction = new Transaction
            {
                Data = DateTime.Now
            };
        }

        public TransactionViewModel(TransactionViewModel toCopy)
            : this()
        {
            Id = toCopy.Id;
            Quantity = toCopy.Quantity;
            Price = toCopy.Price;
            ItemId = toCopy.Id;
            TeamId = toCopy.Id;
            Data = DateTime.Now;
        }

        private string _itemName;
        private string _teamName;

        private Transaction Transaction { get; set; }

        public int Id
        {
            get { return Transaction.Id; }
            set
            {
                Transaction.Id = value;
                RaisePropertyChanged("Id");
            }
        }

        public int Quantity
        {
            get { return Transaction.Quantity; }
            set
            {
                Transaction.Quantity = value;
                RaisePropertyChanged("Quantity");
            }
        }

        public int Price
        {
            get { return Transaction.Price; }
            set
            {
                Transaction.Price = value;
                RaisePropertyChanged("Price");
            }
        }

        public int TeamId
        {
            get { return Transaction.TeamId; }
            set
            {
                Transaction.TeamId = value;
                RaisePropertyChanged("TeamId");
            }
        }

        public int ItemId
        {
            get { return Transaction.ItemId; }
            set
            {
                Transaction.ItemId = value;
                RaisePropertyChanged("ItemId");
            }
        }

        public DateTime Data
        {
            get { return Transaction.Data; }
            set
            {
                Transaction.Data = value;
                RaisePropertyChanged("Data");
            }
        }

        public string ItemName
        {
            get { return _itemName; }
            set
            {
                _itemName = value;
                RaisePropertyChanged("ItemName");
            }
        }

        public string TeamName
        {
            get { return _teamName; }
            set
            {
                _teamName = value;
                RaisePropertyChanged("TeamName");
            }
        }
    }
}