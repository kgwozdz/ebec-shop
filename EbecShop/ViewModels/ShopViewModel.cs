﻿using System;
using System.Linq;
using EbecShop.Helpers;
using EbecShop.Serializers;

namespace EbecShop.ViewModels
{
    public class ShopViewModel : ObservableObject
    {
        #region Constructors

        public ShopViewModel()
        {
            TransactionsViewModel = new TransactionsViewModel();
            TeamsViewModel = new TeamsViewModel();
            ItemsViewModel = new ItemsViewModel();
            CurrentTeam = new TeamViewModel();

            SelectFirsts();
            Quantity = 1;
            ClearCodeAfterBuy = true;
            ClearCodeWhenGotFocus = true;
            SetQuantityAfterBuyToOne = true;
        }

        #endregion Constructors

        #region Members

        private TeamViewModel _currentTeam;
        private int _quantity;
        private string _code;
        private bool _clearCodeAfterBuy;
        private bool _clearCodeWhenGotFocus;
        private bool _setQuantityAfterBuyToOne;

        #endregion Members

        #region Properties

        public TransactionsViewModel TransactionsViewModel { get; set; }
        public TeamsViewModel TeamsViewModel { get; set; }
        public ItemsViewModel ItemsViewModel { get; set; }

        public TeamViewModel CurrentTeam
        {
            get { return _currentTeam; }
            set
            {
                _currentTeam = value;
                TeamsViewModel.CurrentTeam = _currentTeam;
                ItemsViewModel.CurrentTeam = _currentTeam;
                RaisePropertyChanged("CurrentTeam");
            }
        }


        public int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                RaisePropertyChanged("Quantity");
            }
        }

        public string Code
        {
            get { return _code; }
            set
            {
                _code = value;
                if (_code != null && ItemsViewModel != null &&
                    (ItemsViewModel.CurrentItem.Code != _code || ItemsViewModel.CurrentItem == null) &&
                    ItemsViewModel.Items.Any(i => i.Code == _code))
                    ItemsViewModel.CurrentItem = ItemsViewModel.Items.First(i => i.Code == _code);
                RaisePropertyChanged("Code");
            }
        }

        public bool ClearCodeAfterBuy
        {
            get { return _clearCodeAfterBuy; }
            set
            {
                _clearCodeAfterBuy = value;
                RaisePropertyChanged("ClearCodeAfterBuy");
            }
        }

        public bool ClearCodeWhenGotFocus
        {
            get { return _clearCodeWhenGotFocus; }
            set
            {
                _clearCodeWhenGotFocus = value;
                RaisePropertyChanged("ClearCodeWhenGotFocus");
            }
        }

        public bool SetQuantityAfterBuyToOne
        {
            get { return _setQuantityAfterBuyToOne; }
            set
            {
                _setQuantityAfterBuyToOne = value;
                RaisePropertyChanged("SetQuantityAfterBuyToOne");
            }
        }

        #endregion Properties

        #region Commands

        #region RefreshDataCommand

        private RelayCommand _refreshDataCommand;

        public RelayCommand RefreshDataCommand
        {
            get
            {
                return _refreshDataCommand ?? (_refreshDataCommand = new RelayCommand
                    (
                    param => RefreshData(),
                    param => CanRefreshData()
                    ));
            }

            set { _refreshDataCommand = value; }
        }

        private bool CanRefreshData()
        {
            return true;
        }

        #endregion RefreshDataCommand

        #region BuyCommand

        private RelayCommand _buyCommand;

        public RelayCommand BuyCommand
        {
            get
            {
                return _buyCommand ?? (_buyCommand = new RelayCommand
                    (
                    param => Buy(),
                    param => CanBuy()
                    ));
            }
            set { _buyCommand = value; }
        }

        private void Buy()
        {
            var newTransactionViewModel = new TransactionViewModel
            {
                Id = GetNewTransactionId(),
                Quantity = Quantity,
                Price = Quantity*ItemsViewModel.CurrentItem.Price,
                ItemId = ItemsViewModel.CurrentItem.Id,
                TeamId = TeamsViewModel.CurrentTeam.Id,
                Data = DateTime.Now
            };

            TransactionsViewModel.Transactions.Add(newTransactionViewModel);
            ItemsViewModel.CurrentItem.QuantityInWarehouse -= Quantity;
            TeamsViewModel.CurrentTeam.Scores -= Quantity*ItemsViewModel.CurrentItem.Price;

            if (ClearCodeAfterBuy)
                Code = null;
            if (SetQuantityAfterBuyToOne)
                Quantity = 1;

            Save();
            RefreshData();
            SelectEnitites(newTransactionViewModel.Id, newTransactionViewModel.TeamId, newTransactionViewModel.ItemId);
        }

        private bool CanBuy()
        {
            if (Code != "0" && ItemsViewModel.Items.Any(i => i.Code == Code))
                ItemsViewModel.CurrentItem = ItemsViewModel.Items.First(i => i.Code == Code);
            return Quantity > 0 &&
                   ItemsViewModel.CurrentItem != null &&
                   ItemsViewModel.CurrentItem.QuantityInWarehouse >= Quantity &&
                   TeamsViewModel.CurrentTeam != null &&
                   TeamsViewModel.CurrentTeam.Scores >= Quantity*ItemsViewModel.CurrentItem.Price
                   && CurrentTeamCanBuyThisQuantity();
        }

        #endregion BuyCommand

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        public RelayCommand DeleteCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand = new RelayCommand
                    (
                    Delete,
                    param => CanDelete()
                    ));
            }
            set { _deleteCommand = value; }
        }

        private void Delete(object transaction)
        {
            var transactionToDelete = ((string) transaction) == ("last")
                ? TransactionsViewModel.Transactions.OrderByDescending(t => t.Data).First()
                : TransactionsViewModel.CurrentTransaction;
            if (transactionToDelete == null) return;

            if (ItemsViewModel.Items.Any(i => i.Id == transactionToDelete.ItemId))
            {
                var transactionItem = ItemsViewModel.Items.First(param => param.Id == transactionToDelete.ItemId);
                if (transactionItem != null) transactionItem.QuantityInWarehouse += transactionToDelete.Quantity;
            }
            if (TeamsViewModel.Teams.Any(t => t.Id == transactionToDelete.TeamId))
            {
                var transactionTeam = TeamsViewModel.Teams.First(param => param.Id == transactionToDelete.TeamId);
                if (transactionTeam != null) transactionTeam.Scores += transactionToDelete.Price;
            }

            TransactionsViewModel.Transactions.Remove(transactionToDelete);
            Save();
            //RefreshData();

            var selctedTransactionId = TransactionsViewModel.Transactions.Any()
                ? TransactionsViewModel.Transactions.OrderByDescending(t => t.Data).First().Id
                : -1;

            SelectEnitites(selctedTransactionId, transactionToDelete.TeamId, transactionToDelete.ItemId);
        }

        private bool CanDelete()
        {
            return TransactionsViewModel.Transactions.Any() &&
                   TransactionsViewModel.SelectedTransaction != null;
        }

        #endregion DeleteCommand

        #region CodeTextBoxGotFocus

        private RelayCommand _codeTextBoxGotFocusCommand;

        public RelayCommand CodeTextBoxGotFocusCommand
        {
            get
            {
                return _codeTextBoxGotFocusCommand ?? (_codeTextBoxGotFocusCommand = new RelayCommand
                    (
                    param => CodeTextBoxGotFocus(),
                    param => CanCodeTextBoxGotFocus()
                    ));
            }
            set { _codeTextBoxGotFocusCommand = value; }
        }

        private void CodeTextBoxGotFocus()
        {
            Code = null;
        }

        private bool CanCodeTextBoxGotFocus()
        {
            return _clearCodeWhenGotFocus;
        }

        #endregion CodeTextBoxGotFocus

        #endregion Commands

        #region Methods

        public void RefreshData()
        {
            ItemsViewModel.RefreshData();
            TeamsViewModel.RefreshData();
            TransactionsViewModel.RefreshData();
        }

        private bool CurrentTeamCanBuyThisQuantity()
        {
            var boughtQuantity =
                TransactionsViewModel.Transactions.Where(
                    t => t.ItemId == ItemsViewModel.CurrentItem.Id && t.TeamId == TeamsViewModel.CurrentTeam.Id)
                    .Sum(t => t.Quantity);
            return (ItemsViewModel.CurrentItem.MaxQuantityAvailablePerTeam - boughtQuantity) >= Quantity;
        }

        private int GetNewTransactionId()
        {
            return !TransactionsViewModel.Transactions.Any() ? 1 : TransactionsViewModel.Transactions.Max(t => t.Id) + 1;
        }

        private void SelectFirsts()
        {
            if (TransactionsViewModel.Transactions.Any())
                TransactionsViewModel.CurrentTransaction = TransactionsViewModel.Transactions.First();
            if (TeamsViewModel.Teams.Any()) TeamsViewModel.CurrentTeam = TeamsViewModel.Teams.First();
            if (ItemsViewModel.Items.Any()) ItemsViewModel.CurrentItem = ItemsViewModel.Items.First();
        }

        private void SelectEnitites(int transactionId, int teamId, int itemId)
        {
            if (TransactionsViewModel.Transactions.Any())
                TransactionsViewModel.CurrentTransaction =
                    TransactionsViewModel.Transactions.FirstOrDefault(t => t.Id == transactionId);
            if (TeamsViewModel.Teams.Any())
                TeamsViewModel.CurrentTeam = TeamsViewModel.Teams.FirstOrDefault(t => t.Id == teamId);
            if (ItemsViewModel.Items.Any())
                ItemsViewModel.CurrentItem = ItemsViewModel.Items.FirstOrDefault(i => i.Id == itemId);
        }

        private void Save()
        {
            TransactionsSerializer.SerializationFromViewModel(TransactionsViewModel.Transactions);
            ItemsSerializer.SerializationFromViewModel(ItemsViewModel.Items);
            TeamsSerializer.SerializationFromViewModel(TeamsViewModel.Teams);
        }

        #endregion Methods
    }
}