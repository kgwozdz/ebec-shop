﻿using EbecShop.Models;
using EbecShop.Helpers;

namespace EbecShop.ViewModels
{

    public class ItemViewModel : ObservableObject
    {
        public ItemViewModel()
        {
            Item = new Item();
        }

        public ItemViewModel(ItemViewModel toCopy)
            : this()
        {
            Id = toCopy.Id;
            Name = toCopy.Name;
            Price = toCopy.Price;
            Code = toCopy.Code;
            QuantityInWarehouse = toCopy.QuantityInWarehouse;
            MaxQuantityInWarehouse = toCopy.MaxQuantityInWarehouse;
            QuantityAvailablePerTeam = toCopy.QuantityAvailablePerTeam;
            MaxQuantityAvailablePerTeam = toCopy.MaxQuantityAvailablePerTeam;
        }

        private Item Item { get; set; }
        private int _quantityAvailablePerTeam;

        public int Id
        {
            get { return Item.Id; }
            set
            {
                Item.Id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string Name
        {
            get { return Item.Name; }
            set
            {
                Item.Name = value;
                RaisePropertyChanged("Name");
            }
        }

        public int Price
        {
            get { return Item.Price; }
            set
            {
                Item.Price = value;
                RaisePropertyChanged("Price");
            }
        }

        public string Code
        {
            get { return Item.Code; }
            set
            {
                Item.Code = value;
                RaisePropertyChanged("Code");
            }
        }

        public int QuantityInWarehouse
        {
            get { return Item.QuantityInWarehouse; }
            set
            {
                Item.QuantityInWarehouse = value;
                RaisePropertyChanged("QuantityInWarehouse");
            }
        }

        public int MaxQuantityInWarehouse
        {
            get { return Item.MaxQuantityInWarehouse; }
            set
            {
                Item.MaxQuantityInWarehouse = value;
                RaisePropertyChanged("MaxQuantityInWarehouse");
            }
        }

        public int QuantityAvailablePerTeam
        {
            get { return _quantityAvailablePerTeam; }
            set
            {
                _quantityAvailablePerTeam = value;
                RaisePropertyChanged("QuantityAvailablePerTeam");
            }
        }

        public int MaxQuantityAvailablePerTeam
        {
            get { return Item.MaxQuantityAvailablePerTeam; }
            set
            {
                Item.MaxQuantityAvailablePerTeam = value;
                RaisePropertyChanged("MaxQuantityAvailablePerTeam");
            }
        }
    }
}