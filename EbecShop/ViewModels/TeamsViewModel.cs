﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using EbecShop.Helpers;
using EbecShop.Serializers;

namespace EbecShop.ViewModels
{
    public class TeamsViewModel : ObservableObject
    {
        #region Constructors

        public TeamsViewModel()
        {
            Teams = TeamsSerializer.DeserializationToViewModel();
            Members = MembersSerializer.DeserializationToViewModel();
            SetCaptainsNames();
            CurrentTeam = new TeamViewModel();
            SelectedTeam = new TeamViewModel();
            EditMode = false;
            _ascOrderColumn = "Id";
        }

        #endregion Constructors

        #region Members

        private TeamViewModel _currentTeam;
        private TeamViewModel _selectedTeam;
        private ObservableCollection<MemberViewModel> _members;
        private bool _editMode;
        private string _ascOrderColumn;

        #endregion Members

        #region Properties

        public ObservableCollection<TeamViewModel> Teams { get; set; }

        public TeamViewModel CurrentTeam
        {
            get { return _currentTeam; }
            set
            {
                _currentTeam = value;
                RaisePropertyChanged("CurrentTeam");
            }
        }

        public TeamViewModel SelectedTeam
        {
            get { return _selectedTeam; }
            set
            {
                _selectedTeam = value;
                RaisePropertyChanged("SelectedTeam");
            }
        }

        public ObservableCollection<MemberViewModel> Members
        {
            get { return _members; }
            set
            {
                _members = value;
                RaisePropertyChanged("Members");
            }
        }

        public bool EditMode
        {
            get { return _editMode; }
            set
            {
                _editMode = value;
                RaisePropertyChanged("EditMode");
            }
        }

        #endregion Properties

        #region Commands

        #region EditTeamCommand

        private RelayCommand _editTeamCommand;

        public RelayCommand EditTeamCommand
        {
            get
            {
                return _editTeamCommand ?? (_editTeamCommand = new RelayCommand
                    (
                    param => Edit(),
                    param => CanEdit()
                    ));
            }
            set { _editTeamCommand = value; }
        }

        private void Edit()
        {
            CurrentTeam = new TeamViewModel(SelectedTeam);
            EditMode = true;
        }

        private bool CanEdit()
        {
            return !EditMode &&
                   SelectedTeam != null &&
                   SelectedTeam.Id != 0;
        }

        #endregion EditTeamCommand

        #region SaveTeamEditCommand

        private RelayCommand _saveTeamEditCommand;

        public RelayCommand SaveTeamEditCommand
        {
            get
            {
                return _saveTeamEditCommand ?? (_saveTeamEditCommand = new RelayCommand
                    (
                    param => SaveEdit(),
                    param => CanSaveEdit()
                    ));
            }
            set { _saveTeamEditCommand = value; }
        }

        private void SaveEdit()
        {
            SelectedTeam = Teams.First(t => t.Id == CurrentTeam.Id);

            SelectedTeam.Name = CurrentTeam.Name;
            SelectedTeam.Scores = CurrentTeam.Scores;
            SelectedTeam.CaptainId = CurrentTeam.CaptainId;

            CurrentTeam = new TeamViewModel();
            EditMode = false;
            Save();
            RefreshData();
        }

        private bool CanSaveEdit()
        {
            return IsValid(CurrentTeam) &&
                   TeamWithThisIdAlreadyExists(CurrentTeam.Id) &&
                   EditMode;
        }

        #endregion SaveTeamEditCommand

        #region CancelTeamEditCommand

        private RelayCommand _cancelTeamEditCommand;

        public RelayCommand CancelTeamEditCommand
        {
            get
            {
                return _cancelTeamEditCommand ?? (_cancelTeamEditCommand = new RelayCommand
                    (
                    param => CancelEdit(),
                    param => CanCancelEdit()
                    ));
            }
            set { _cancelTeamEditCommand = value; }
        }

        private void CancelEdit()
        {
            CurrentTeam = new TeamViewModel();
            EditMode = false;
        }

        private bool CanCancelEdit()
        {
            return EditMode;
        }

        #endregion SaveTeamEditCommand

        #region AddTeamCommand

        private RelayCommand _addTeamCommand;

        public RelayCommand AddTeamCommand
        {
            get
            {
                return _addTeamCommand ?? (_addTeamCommand = new RelayCommand
                    (
                    param => AddTeam(),
                    param => CanAddTeam()
                    ));
            }
            set { _addTeamCommand = value; }
        }

        private void AddTeam()
        {
            Teams.Add(CurrentTeam);
            Save();
            RefreshData();
            SelectedTeam = Teams.First(t => t.Id == CurrentTeam.Id);
            CurrentTeam = new TeamViewModel();
        }

        private bool CanAddTeam()
        {
            return IsValid(CurrentTeam) &&
                   !TeamWithThisIdAlreadyExists(CurrentTeam.Id) &&
                   !EditMode;
        }

        #endregion AddTeamCommand

        #region DeleteTeamCommand

        private RelayCommand _deleteTeamCommand;

        public RelayCommand DeleteTeamCommand
        {
            get
            {
                return _deleteTeamCommand ?? (_deleteTeamCommand = new RelayCommand
                    (
                    param => DeleteTeam(),
                    param => CanDeleteTeam()
                    ));
            }
            set { _deleteTeamCommand = value; }
        }

        private void DeleteTeam()
        {
            Teams.Remove(SelectedTeam);
            Save();
            CurrentTeam = new TeamViewModel();
        }

        private bool CanDeleteTeam()
        {
            return TeamAlreadyExists(SelectedTeam) &&
                   !EditMode;
        }

        #endregion DeleteTeamCommand

        #region ClearFormCommand

        private RelayCommand _clearFormCommand;

        public RelayCommand ClearFormCommand
        {
            get
            {
                return _clearFormCommand ?? (_clearFormCommand = new RelayCommand
                    (
                    param => ClearForm(),
                    param => CanClearForm()
                    ));
            }
            set { _clearFormCommand = value; }
        }

        private void ClearForm()
        {
            CurrentTeam = new TeamViewModel();
        }

        private bool CanClearForm()
        {
            return !EditMode;
        }

        #endregion ClearFormCommand

        #region RemoveAllTeamsCommand

        private RelayCommand _removeAllTeamsCommand;

        public RelayCommand RemoveAllTeamsCommand
        {
            get
            {
                return _removeAllTeamsCommand ?? (_removeAllTeamsCommand = new RelayCommand
                    (
                    param => RemoveAllTeams(),
                    param => CanRemoveAllTeams()
                    ));
            }
            set { _removeAllTeamsCommand = value; }
        }

        private void RemoveAllTeams()
        {
            if (mBoxService.AskForConfirmation("Do you want to remove all teams?", "Confirmation"))
            {
                Teams.Clear();
                Save();
                CurrentTeam = new TeamViewModel();
                SelectedTeam = new TeamViewModel();
                EditMode = false;
            }
        }

        private bool CanRemoveAllTeams()
        {
            return Teams.Any() &&
                   !EditMode;
        }

        #endregion RemoveAllTeamsCommand

        #region RefreshDataCommand

        private RelayCommand _refreshDataCommand;

        public RelayCommand RefreshDataCommand
        {
            get
            {
                return _refreshDataCommand ?? (_refreshDataCommand = new RelayCommand
                    (
                    param => RefreshData(),
                    param => CanRefreshData()
                    ));
            }

            set { _refreshDataCommand = value; }
        }

        private bool CanRefreshData()
        {
            return true;
        }

        #endregion RefreshDataCommand

        #region SortTeamCommand

        private RelayCommand _sortTeamCommand;

        public RelayCommand SortTeamCommand
        {
            get
            {
                return _sortTeamCommand ?? (_sortTeamCommand = new RelayCommand
                    (
                    Sort,
                    param => CanSort()
                    ));
            }
            set { _sortTeamCommand = value; }
        }

        private void Sort(object parametr)
        {
            if (!(parametr is string))
                return;

            var teamId = SelectedTeam != null ? SelectedTeam.Id : -1;
            var column = (string) parametr;

            if (column != _ascOrderColumn)
            {
                SortAsc(SelectSelector(column));
                _ascOrderColumn = column;
            }
            else
            {
                SortDesc(SelectSelector(column));
                _ascOrderColumn = "";
            }

            Save();
            SelectedTeam = Teams.FirstOrDefault(i => i.Id == teamId);

            RaisePropertyChanged("Teams");
        }

        private Func<TeamViewModel, object> SelectSelector(string columnName)
        {
            switch (columnName)
            {
                case "Id":
                    return (i => i.Id);
                case "Name":
                    return (i => i.Name);
                case "Scores":
                    return (i => i.Scores);
                case "CaptainId":
                    return (i => i.CaptainId);
                case "CaptainName":
                    return (i => i.CaptainName);
                default:
                    return (i => i.Id);
            }
        }

        private void SortAsc(Func<TeamViewModel, object> selector)
        {
            Teams = new ObservableCollection<TeamViewModel>(Teams.OrderBy(selector));
        }

        private void SortDesc(Func<TeamViewModel, object> selector)
        {
            Teams = new ObservableCollection<TeamViewModel>(Teams.OrderByDescending(selector));
        }

        private bool CanSort()
        {
            return true;
        }

        #endregion SortTeamCommand

        #endregion Commmands

        #region Methods

        public void Save()
        {
            TeamsSerializer.SerializationFromViewModel(Teams);
        }

        public void RefreshData()
        {
            RefreshTeams();
            RefreshMembers();
            SetCaptainsNames();
        }

        private void RefreshTeams()
        {
            try
            {
                //TODO SORTING AFTER READ
                //int teamId = SelectedTeam != null ? SelectedTeam.Id : -1;
                Teams = TeamsSerializer.DeserializationToViewModel();
                //Teams = new ObservableCollection<TeamViewModel>(Teams.OrderBy(i => i.Id));
                //SelectedTeam = Teams.First(t => t.Id == teamId);
            }
            catch (Exception)
            {
                // ignored
            }
            RaisePropertyChanged("Teams");
        }

        private void RefreshMembers()
        {
            try
            {
                Members = MembersSerializer.DeserializationToViewModel();
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private bool IsValid(TeamViewModel team)
        {
            return team.Id > 0 &&
                   !String.IsNullOrWhiteSpace(team.Name) &&
                   team.CaptainId > 0 &&
                   team.Scores >= 0;
        }

        private bool TeamWithThisIdAlreadyExists(int teamId)
        {
            return Teams.Any(team => team.Id == teamId);
        }

        private bool TeamAlreadyExists(TeamViewModel team)
        {
            return Teams.Any(t => t.Equals(team));
        }

        private void SetCaptainsNames()
        {
            foreach (var team in Teams.Where(team => Members.Any(m => m.Id == team.CaptainId)))
            {
                team.CaptainName = Members.First(m => m.Id == team.CaptainId).FirstName +
                                   " " +
                                   Members.First(m => m.Id == team.CaptainId).LastName;
            }
        }

        #endregion Methods
    }
}