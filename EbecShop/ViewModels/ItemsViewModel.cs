﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using EbecShop.Helpers;
using EbecShop.Serializers;

namespace EbecShop.ViewModels
{
    public class ItemsViewModel : ObservableObject
    {
        #region Constructors

        public ItemsViewModel()
        {
            Items = ItemsSerializer.DeserializationToViewModel();
            Transactions = TransactionsSerializer.DeserializationToViewModel();
            CurrentItem = new ItemViewModel();
            SelectedItem = new ItemViewModel();
            CurrentTeam = new TeamViewModel();
            SetQuantityAvailablePerTeam();
            EditMode = false;

            MaxAmountIsEqualToCurrent = false;
            _ascOrderColumn = "Id";
        }

        #endregion Constructors

        #region Members

        private ItemViewModel _currentItem;
        private ItemViewModel _selectedItem;
        private ObservableCollection<TransactionViewModel> _transactions;
        private TeamViewModel _currentTeam;
        private string _ascOrderColumn;
        private bool _editMode;
        private bool _maxAmountIsEqualToCurrent;

        #endregion Members

        #region Properties

        public ObservableCollection<ItemViewModel> Items { get; set; }

        public ItemViewModel CurrentItem
        {
            get { return _currentItem; }
            set
            {
                _currentItem = value;
                RaisePropertyChanged("CurrentItem");
            }
        }

        public ItemViewModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged("SelectedItem");
            }
        }

        public ObservableCollection<TransactionViewModel> Transactions
        {
            get { return _transactions; }
            set
            {
                _transactions = value;
                RaisePropertyChanged("Transactions");
            }
        }

        public TeamViewModel CurrentTeam
        {
            get { return _currentTeam; }
            set
            {
                _currentTeam = value;
                RaisePropertyChanged("CurrentTeam");
            }
        }

        public bool EditMode
        {
            get { return _editMode; }
            set
            {
                _editMode = value;
                RaisePropertyChanged("EditMode");
            }
        }


        public bool MaxAmountIsEqualToCurrent
        {
            get { return _maxAmountIsEqualToCurrent; }
            set
            {
                _maxAmountIsEqualToCurrent = value;
                RaisePropertyChanged("MaxAmountIsEqualToCurrent");
            }
        }

        #endregion Properties

        #region Commands

        #region EditItemCommand

        private RelayCommand _editItemCommand;

        public RelayCommand EditItemCommand
        {
            get
            {
                return _editItemCommand ?? (_editItemCommand = new RelayCommand
                    (
                    param => Edit(),
                    param => CanEdit()
                    ));
            }
            set { _editItemCommand = value; }
        }

        private void Edit()
        {
            CurrentItem = new ItemViewModel(SelectedItem);
            EditMode = true;
        }

        private bool CanEdit()
        {
            return !EditMode &&
                   SelectedItem != null &&
                   SelectedItem.Id != 0;
        }

        #endregion EditItemCommand

        #region SaveItemEditCommand

        private RelayCommand _saveItemEditCommand;

        public RelayCommand SaveItemEditCommand
        {
            get
            {
                return _saveItemEditCommand ?? (_saveItemEditCommand = new RelayCommand
                    (
                    param => SaveEdit(),
                    param => CanSaveEdit()
                    )
                    );
            }
            set { _saveItemEditCommand = value; }
        }

        private void SaveEdit()
        {
            SelectedItem = Items.First(m => m.Id == CurrentItem.Id);

            SelectedItem.Name = CurrentItem.Name;
            SelectedItem.Price = CurrentItem.Price;
            SelectedItem.Code = CurrentItem.Code;
            SelectedItem.QuantityInWarehouse = CurrentItem.QuantityInWarehouse;
            SelectedItem.MaxQuantityInWarehouse = CurrentItem.MaxQuantityInWarehouse;
            SelectedItem.MaxQuantityAvailablePerTeam = CurrentItem.MaxQuantityAvailablePerTeam;

            CurrentItem = new ItemViewModel();
            EditMode = false;
            Save();
            RefreshData();
        }

        private bool CanSaveEdit()
        {
            return IsValid(CurrentItem) &&
                   ItemWithThisIdAlreadyExists(CurrentItem.Id) &&
                   (!ItemWithThisCodeAlreadyExists(CurrentItem.Code) || CurrentItem.Code == SelectedItem.Code) &&
                   EditMode;
        }

        #endregion SaveItemEditCommand

        #region CancelItemEditCommand

        private RelayCommand _cancelItemEditCommand;

        public RelayCommand CancelItemEditCommand
        {
            get
            {
                return _cancelItemEditCommand ?? (_cancelItemEditCommand = new RelayCommand
                    (
                    param => CancelEdit(),
                    param => CanCancelEdit()
                    ));
            }
            set { _cancelItemEditCommand = value; }
        }

        private void CancelEdit()
        {
            CurrentItem = new ItemViewModel();
            EditMode = false;
        }

        private bool CanCancelEdit()
        {
            return EditMode;
        }

        #endregion SaveItemEditCommand

        #region AddItemCommand

        private RelayCommand _addItemCommand;

        public RelayCommand AddItemCommand
        {
            get
            {
                return _addItemCommand ?? (_addItemCommand = new RelayCommand
                    (
                    param => AddItem(),
                    param => CanAddItem()
                    ));
            }
            set { _addItemCommand = value; }
        }

        private void AddItem()
        {
            Items.Add(CurrentItem);
            Save();
            RefreshData();
            SelectedItem = Items.First(i => i.Id == CurrentItem.Id);
            CurrentItem = new ItemViewModel();
        }

        private bool CanAddItem()
        {
            return IsValid(CurrentItem) &&
                   !ItemWithThisIdAlreadyExists(CurrentItem.Id) &&
                   !ItemWithThisCodeAlreadyExists(CurrentItem.Code) &&
                   !EditMode;
        }

        #endregion AddItemCommand

        #region DeleteItemCommand

        private RelayCommand _deleteItemCommand;

        public RelayCommand DeleteItemCommand
        {
            get
            {
                return _deleteItemCommand ?? (_deleteItemCommand = new RelayCommand
                    (
                    param => DeleteItem(),
                    param => CanDeleteItem()
                    ));
            }
            set { _deleteItemCommand = value; }
        }

        private void DeleteItem()
        {
            Items.Remove(SelectedItem);
            Save();
            SelectedItem = new ItemViewModel();
        }

        private bool CanDeleteItem()
        {
            return ItemAlreadyExists(SelectedItem) &&
                   !EditMode;
        }

        #endregion DeleteItemCommand

        #region ClearFormCommand

        private RelayCommand _clearFormCommand;

        public RelayCommand ClearFormCommand
        {
            get
            {
                return _clearFormCommand ?? (_clearFormCommand = new RelayCommand
                    (
                    param => ClearForm(),
                    param => CanClearForm()
                    ));
            }
            set { _clearFormCommand = value; }
        }

        private void ClearForm()
        {
            CurrentItem = new ItemViewModel();
        }

        private bool CanClearForm()
        {
            return !EditMode;
        }

        #endregion ClearFormCommand

        #region RemoveAllItemsCommand

        private RelayCommand _removeAllItemsCommand;

        public RelayCommand RemoveAllItemsCommand
        {
            get
            {
                return _removeAllItemsCommand ?? (_removeAllItemsCommand = new RelayCommand
                    (
                    param => RemoveAllItems(),
                    param => CanRemoveAllItems()
                    ));
            }
            set { _removeAllItemsCommand = value; }
        }

        private void RemoveAllItems()
        {
            if (mBoxService.AskForConfirmation("Do you want to remove all items?", "Confirmation"))
            {
                Items.Clear();
                Save();
                CurrentItem = new ItemViewModel();
                SelectedItem = new ItemViewModel();
                EditMode = false;
            }
        }

        private bool CanRemoveAllItems()
        {
            return Items.Any() && !EditMode;
        }

        #endregion RemoveAllItemsCommand

        #region RefreshDataCommand

        private RelayCommand _refreshDataCommand;

        public RelayCommand RefreshDataCommand
        {
            get
            {
                return _refreshDataCommand ?? (_refreshDataCommand = new RelayCommand
                    (
                    param => RefreshData(),
                    param => CanRefreshData()
                    ));
            }

            set { _refreshDataCommand = value; }
        }

        private bool CanRefreshData()
        {
            return true;
        }

        #endregion RefreshDataCommand

        #region SortItemCommand

        private RelayCommand _sortItemCommand;

        public RelayCommand SortItemCommand
        {
            get
            {
                return _sortItemCommand ?? (_sortItemCommand = new RelayCommand
                    (
                    Sort,
                    param => CanSort()
                    ));
            }
            set { _sortItemCommand = value; }
        }

        private void Sort(object parametr)
        {
            if (!(parametr is string))
                return;

            var itemId = SelectedItem != null ? SelectedItem.Id : -1;
            var column = (string) parametr;

            if (column != _ascOrderColumn)
            {
                SortAsc(SelectSelector(column));
                _ascOrderColumn = column;
            }
            else
            {
                SortDasc(SelectSelector(column));
                _ascOrderColumn = "";
            }

            Save();
            SelectedItem = Items.FirstOrDefault(i => i.Id == itemId);

            RaisePropertyChanged("Items");
        }

        private Func<ItemViewModel, object> SelectSelector(string columnName)
        {
            switch (columnName)
            {
                case "Id":
                    return (i => i.Id);
                case "Name":
                    return (i => i.Name);
                case "Price":
                    return (i => i.Price);
                case "Code":
                    return (i => i.Code);
                case "QuantityInWarehouse":
                    return (i => i.QuantityInWarehouse);
                case "MaxQuantityInWarehouse":
                    return (i => i.MaxQuantityInWarehouse);
                case "QuantityAvailablePerTeam":
                    return (i => i.QuantityAvailablePerTeam);
                case "MaxQuantityAvailablePerTeam":
                    return (i => i.MaxQuantityAvailablePerTeam);
                default:
                    return (i => i.Id);
            }
        }

        private void SortAsc(Func<ItemViewModel, object> selector)
        {
            Items = new ObservableCollection<ItemViewModel>(Items.OrderBy(selector));
        }

        private void SortDasc(Func<ItemViewModel, object> selector)
        {
            Items = new ObservableCollection<ItemViewModel>(Items.OrderByDescending(selector));
        }

        private bool CanSort()
        {
            return true;
        }

        #endregion SortItemCommand

        #endregion Commands

        #region Methods

        public void Save()
        {
            ItemsSerializer.SerializationFromViewModel(Items);
        }

        public void RefreshData()
        {
            RefreshItems();
            RefreshTransactions();
            SetQuantityAvailablePerTeam();
        }

        private void RefreshItems()
        {
            try
            {
                //TODO SORTING AFTER READ
                //int itemId = SelectedItem.Id;
                Items = ItemsSerializer.DeserializationToViewModel();
                //Items = new ObservableCollection<ItemViewModel>(Items.OrderBy(i => i.Id));
                //SelectedItem = Items.First(i => i.Id == itemId);
            }
            catch (Exception)
            {
                // ignored
            }
            RaisePropertyChanged("Items");
        }

        private void RefreshTransactions()
        {
            try
            {
                Transactions = TransactionsSerializer.DeserializationToViewModel();
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private bool IsValid(ItemViewModel item)
        {
            return item.Id > 0 &&
                   !String.IsNullOrWhiteSpace(item.Name) &&
                   item.Price >= 0 &&
                   !string.IsNullOrEmpty(item.Code) &&
                   item.MaxQuantityAvailablePerTeam >= 0 &&
                   item.QuantityInWarehouse >= 0 &&
                   item.MaxQuantityInWarehouse >= 0;
        }

        private bool ItemWithThisIdAlreadyExists(int id)
        {
            return Items.Any(item => item.Id == id);
        }

        private bool ItemWithThisCodeAlreadyExists(string code)
        {
            return Items.Any(param => param.Code.Equals(code));
        }

        private bool ItemAlreadyExists(ItemViewModel item)
        {
            return Items.Any(i => i.Equals(item));
        }

        private void SetQuantityAvailablePerTeam()
        {
            //TODO Dodać QuantityAvailablePerTeam do walidacji

            foreach (var item in Items)
            {
                item.QuantityAvailablePerTeam = item.MaxQuantityAvailablePerTeam;
            }

            foreach (var item in Items.Where(i => Transactions.Any(t => t.ItemId == i.Id)))
            {
                item.QuantityAvailablePerTeam = item.MaxQuantityAvailablePerTeam -
                                                Transactions.Where(
                                                    t => t.ItemId == item.Id && t.TeamId == CurrentTeam.Id)
                                                    .Sum(s => s.Quantity);
            }
        }

        #endregion Methods
    }
}