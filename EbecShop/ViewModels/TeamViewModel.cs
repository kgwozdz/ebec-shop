﻿using EbecShop.Helpers;
using EbecShop.Models;

namespace EbecShop.ViewModels
{
    public class TeamViewModel : ObservableObject
    {
        #region Constructors

        public TeamViewModel()
        {
            Team = new Team();
        }

        public TeamViewModel(TeamViewModel toCopy)
            : this()
        {
            Id = toCopy.Id;
            Name = toCopy.Name;
            Scores = toCopy.Scores;
            CaptainId = toCopy.CaptainId;
        }

        #endregion Constructors

        #region Members

        private string _captainName;

        #endregion

        #region Properties

        private Team Team { get; set; }

        public int Id
        {
            get { return Team.Id; }
            set
            {
                Team.Id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string Name
        {
            get { return Team.Name; }
            set
            {
                Team.Name = value;
                RaisePropertyChanged("Name");
            }
        }

        public int CaptainId
        {
            get { return Team.CaptainId; }
            set
            {
                Team.CaptainId = value;
                RaisePropertyChanged("CaptainId");
            }
        }

        public int Scores
        {
            get { return Team.Scores; }
            set
            {
                Team.Scores = value;
                RaisePropertyChanged("Scores");
            }
        }

        public string CaptainName
        {
            get { return _captainName; }
            set
            {
                _captainName = value;
                RaisePropertyChanged("CaptainName");
            }
        }

        #endregion Properties
    }
}