﻿using EbecShop.Helpers;

namespace EbecShop.ViewModels
{
    public class ContactViewModel : ObservableObject
    {
        #region Constructors

        public ContactViewModel()
        {
            Author = "Krzysztof Gwóźdź";
            Email = "krzysztof.gwozdz@bestgliwice.pl";
            Number = 669043854;
        }

        #endregion Constructors

        #region Members

        private string _author;
        private string _email;
        private int _number;

        #endregion Members

        #region Properties

        public string Author
        {
            get { return _author; }
            set
            {
                _author = value;
                RaisePropertyChanged("Author");
            }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                RaisePropertyChanged("Email");
            }
        }

        public int Number
        {
            get { return _number; }
            set
            {
                _number = value;
                RaisePropertyChanged("Number");
            }
        }

        #endregion Properties
    }
}