﻿using EbecShop.Helpers;
using EbecShop.Models;

namespace EbecShop.ViewModels
{
    public class MemberViewModel : ObservableObject
    {

        public MemberViewModel()
        {
            Member = new Member();
        }

        public MemberViewModel(MemberViewModel toCopy)
            : this()
        {
            Id = toCopy.Id;
            FirstName = toCopy.FirstName;
            LastName = toCopy.LastName;
            TeamId = toCopy.TeamId;
        }

        private string _teamName;

        private Member Member { get; set; }

        public int Id
        {
            get { return Member.Id; }
            set
            {
                Member.Id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string FirstName
        {
            get { return Member.FirstName; }
            set
            {
                Member.FirstName = value;
                RaisePropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get { return Member.LastName; }
            set
            {
                Member.LastName = value;
                RaisePropertyChanged("LastName");
            }
        }

        public int TeamId
        {
            get { return Member.TeamId; }
            set
            {
                Member.TeamId = value;
                RaisePropertyChanged("TeamId");
            }
        }

        public string TeamName
        {
            get { return _teamName; }
            set
            {
                _teamName = value;
                RaisePropertyChanged("TeamName");
            }
        }
    }
}