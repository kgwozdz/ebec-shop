﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;
using EbecShop.Models;
using EbecShop.ViewModels;

namespace EbecShop.Serializers
{
    public static class TransactionsSerializer
    {
        public static void Serialize(Transactions transactions)
        {
            var serializer = new XmlSerializer(typeof (Transactions));
            try
            {
                if (!Directory.Exists(ApplicationSettings.DataDiretory))
                    Directory.CreateDirectory(ApplicationSettings.DataDiretory);
                using (var writer = new StreamWriter(ApplicationSettings.DataDiretory + @"\Transactions.xml"))
                {
                    serializer.Serialize(writer, transactions);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static Transactions FromViewModelToCollection(ObservableCollection<TransactionViewModel> viewmodel)
        {
            var transactions = new Transactions();
            foreach (var transaction in viewmodel)
            {
                transactions.Add(new Transaction
                {
                    Id = transaction.Id,
                    Quantity = transaction.Quantity,
                    Price = transaction.Price,
                    TeamId = transaction.TeamId,
                    ItemId = transaction.ItemId,
                    Data = transaction.Data
                });
            }
            return transactions;
        }

        public static void SerializationFromViewModel(ObservableCollection<TransactionViewModel> viewmodel)
        {
            var serializedTransactions = FromViewModelToCollection(viewmodel);
            Serialize(serializedTransactions);
        }

        public static Transactions Deserialization()
        {
            var serializer = new XmlSerializer(typeof (Transactions));
            var newTransactions = new Transactions();
            try
            {
                using (
                    var fileStream = new FileStream(ApplicationSettings.DataDiretory + @"\Transactions.xml",
                        FileMode.Open))
                {
                    newTransactions = (Transactions) serializer.Deserialize(fileStream);
                }
            }
            catch (Exception)
            {
                // ignored
            }
            return newTransactions;
        }

        public static ObservableCollection<TransactionViewModel> FromCollectionToViewModel(Transactions transactions)
        {
            var viewModel = new ObservableCollection<TransactionViewModel>();
            foreach (Transaction transaction in transactions)
            {
                viewModel.Add(new TransactionViewModel
                {
                    Id = transaction.Id,
                    Quantity = transaction.Quantity,
                    Price = transaction.Price,
                    TeamId = transaction.TeamId,
                    ItemId = transaction.ItemId,
                    Data = transaction.Data
                });
            }
            return viewModel;
        }

        public static ObservableCollection<TransactionViewModel> DeserializationToViewModel()
        {
            var serializedTransactions = Deserialization();
            return FromCollectionToViewModel(serializedTransactions);
        }
    }
}