﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;
using EbecShop.Models;
using EbecShop.ViewModels;

namespace EbecShop.Serializers
{
    public static class MembersSerializer
    {
        public static void Serialize(Members members)
        {
            var serializer = new XmlSerializer(typeof (Members));
            try
            {
                if (!Directory.Exists(ApplicationSettings.DataDiretory))
                    Directory.CreateDirectory(ApplicationSettings.DataDiretory);
                using (var writer = new StreamWriter(ApplicationSettings.DataDiretory + @"\Members.xml"))
                {
                    serializer.Serialize(writer, members);
                    ;
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static Members FromViewModelToCollection(ObservableCollection<MemberViewModel> viewmodel)
        {
            var members = new Members();
            foreach (var member in viewmodel)
            {
                members.Add(new Member
                {
                    FirstName = member.FirstName,
                    LastName = member.LastName,
                    Id = member.Id,
                    TeamId = member.TeamId
                });
            }
            return members;
        }

        public static void SerializationFromViewModel(ObservableCollection<MemberViewModel> viewmodel)
        {
            var serializedMembers = FromViewModelToCollection(viewmodel);
            Serialize(serializedMembers);
        }

        public static Members Deserialization()
        {
            var newMembers = new Members();
            var serializer = new XmlSerializer(typeof (Members));
            try
            {
                using (
                    var fileStream = new FileStream(ApplicationSettings.DataDiretory + @"\Members.xml",
                        FileMode.Open))
                {
                    newMembers = (Members) serializer.Deserialize(fileStream);
                }
            }
            catch (Exception)
            {
                // ignored
            }
            return newMembers;
        }

        public static ObservableCollection<MemberViewModel> FromCollectionToViewModel(Members members)
        {
            var viewModel = new ObservableCollection<MemberViewModel>();
            foreach (Member member in members)
            {
                viewModel.Add(new MemberViewModel
                {
                    FirstName = member.FirstName,
                    LastName = member.LastName,
                    Id = member.Id,
                    TeamId = member.TeamId
                });
            }
            return viewModel;
        }

        public static ObservableCollection<MemberViewModel> DeserializationToViewModel()
        {
            var serializedMembers = Deserialization();
            return FromCollectionToViewModel(serializedMembers);
        }
    }
}