﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;
using EbecShop.Models;
using EbecShop.ViewModels;

namespace EbecShop.Serializers
{
    public static class TeamsSerializer
    {
        public static void Serialize(Teams teams)
        {
            var serializer = new XmlSerializer(typeof (Teams));
            try
            {
                if (!Directory.Exists(ApplicationSettings.DataDiretory))
                    Directory.CreateDirectory(ApplicationSettings.DataDiretory);
                using (var writer = new StreamWriter(ApplicationSettings.DataDiretory + @"\Teams.xml"))
                {
                    serializer.Serialize(writer, teams);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static Teams FromViewModelToCollection(ObservableCollection<TeamViewModel> viewmodel)
        {
            var teams = new Teams();
            foreach (var team in viewmodel)
            {
                teams.Add(new Team
                {
                    Id = team.Id,
                    Name = team.Name,
                    Scores = team.Scores,
                    CaptainId = team.CaptainId
                });
            }
            return teams;
        }

        public static void SerializationFromViewModel(ObservableCollection<TeamViewModel> viewmodel)
        {
            var serializedTeams = FromViewModelToCollection(viewmodel);
            Serialize(serializedTeams);
        }

        public static Teams Deserialization()
        {
            var serializer = new XmlSerializer(typeof (Teams));
            var newTeams = new Teams();
            try
            {
                using (
                    var fileStream = new FileStream(ApplicationSettings.DataDiretory + @"\Teams.xml",
                        FileMode.Open))
                {
                    newTeams = (Teams) serializer.Deserialize(fileStream);
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return newTeams;
        }

        public static ObservableCollection<TeamViewModel> FromCollectionToViewModel(Teams teams)
        {
            var viewModel = new ObservableCollection<TeamViewModel>();
            foreach (Team team in teams)
            {
                viewModel.Add(new TeamViewModel
                {
                    Id = team.Id,
                    Name = team.Name,
                    Scores = team.Scores,
                    CaptainId = team.CaptainId
                });
            }
            return viewModel;
        }

        public static ObservableCollection<TeamViewModel> DeserializationToViewModel()
        {
            var serializedTeams = Deserialization();
            return FromCollectionToViewModel(serializedTeams);
        }
    }
}