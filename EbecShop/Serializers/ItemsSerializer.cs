﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;
using EbecShop.Models;
using EbecShop.ViewModels;

namespace EbecShop.Serializers
{
    public static class ItemsSerializer
    {
        public static void Serialize(Items items)
        {
            var serializer = new XmlSerializer(typeof (Items));
            try
            {
                if (!Directory.Exists(ApplicationSettings.DataDiretory))
                    Directory.CreateDirectory(ApplicationSettings.DataDiretory);
                using (var writer = new StreamWriter(ApplicationSettings.DataDiretory + @"\Items.xml"))
                {
                    serializer.Serialize(writer, items);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static Items FromViewModelToCollection(ObservableCollection<ItemViewModel> viewmodel)
        {
            var items = new Items();
            foreach (var item in viewmodel)
            {
                items.Add(new Item
                {
                    Id = item.Id,
                    Name = item.Name,
                    Price = item.Price,
                    Code = item.Code,
                    QuantityInWarehouse = item.QuantityInWarehouse,
                    MaxQuantityInWarehouse = item.MaxQuantityInWarehouse,
                    MaxQuantityAvailablePerTeam = item.MaxQuantityAvailablePerTeam
                });
            }
            return items;
        }

        public static void SerializationFromViewModel(ObservableCollection<ItemViewModel> viewmodel)
        {
            var serializedItems = FromViewModelToCollection(viewmodel);
            Serialize(serializedItems);
        }

        public static Items Deserialization()
        {
            var serializer = new XmlSerializer(typeof (Items));
            var newItems = new Items();
            try
            {
                using (
                    var fileStream = new FileStream(ApplicationSettings.DataDiretory + @"\Items.xml",
                        FileMode.Open))
                {
                    newItems = (Items) serializer.Deserialize(fileStream);
                }
            }
            catch (Exception)
            {
                // ignored
            }
            return newItems;
        }

        public static ObservableCollection<ItemViewModel> FromCollectionToViewModel(Items items)
        {
            var viewModel = new ObservableCollection<ItemViewModel>();
            foreach (Item item in items)
            {
                viewModel.Add(new ItemViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Price = item.Price,
                    Code = item.Code,
                    QuantityInWarehouse = item.QuantityInWarehouse,
                    MaxQuantityInWarehouse = item.MaxQuantityInWarehouse,
                    MaxQuantityAvailablePerTeam = item.MaxQuantityAvailablePerTeam
                });
            }
            return viewModel;
        }

        public static ObservableCollection<ItemViewModel> DeserializationToViewModel()
        {
            var serializedItems = Deserialization();
            return FromCollectionToViewModel(serializedItems);
        }
    }
}