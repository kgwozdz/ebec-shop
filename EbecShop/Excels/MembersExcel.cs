﻿using System;
using System.Reflection;
using Microsoft.Office.Interop.Excel;

namespace EbecShop.Excels
{
    internal class MembersExcel
    {
        public static void Write()
        {
            try
            {
                object misValue = Missing.Value;
                var MyApp = new Application();
                if (MyApp == null) return;
                MyApp.Visible = false;

                var MyBook = MyApp.Workbooks.Add(misValue);

                var MySheet = (Worksheet) MyBook.Worksheets.get_Item(1);
                MySheet.Name = "Members";

                MySheet.Cells[1, 1] = "jeden";
                MySheet.Cells[1, 2] = "dwa";
                MySheet.Cells[1, 3] = "trzy";
                MySheet.Cells[1, 4] = "cztery";

                MyBook.SaveAs("d:\\EbecShop.xls", XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue,
                    XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                MyBook.Close(true, misValue, misValue);
                MyApp.Quit();
            }
            catch (Exception)
            {
            }
        }
    }
}