﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace EbecShop.Helpers
{
    [ValueConversion(typeof(object), typeof(string))]
    public class PositiveNumberString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int? number = value as int?;
            if (number != null && number < 0)
            {
                return String.Empty;
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string text = value as string;
            if (String.IsNullOrWhiteSpace(text))
            {
                return -1;
            }
            else
            {
                try
                {
                    return Int32.Parse(text);
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }
    }

    [ValueConversion(typeof(DateTime), typeof(string))]
    public class DateTimeFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is DateTime)
            {
                return ((DateTime)value).ToString("yyyy.MM.dd  HH:mm:ss");
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    [ValueConversion(typeof(bool), typeof(bool))]
    public class LogicNegator : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is bool)
                return !((bool)value);
            else return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}