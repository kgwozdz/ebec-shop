﻿using System;
using System.Collections;

namespace EbecShop.Models
{
    public class Members : ICollection
    {
        private readonly ArrayList _members = new ArrayList();

        public Member this[int index]
        {
            get { return (Member) _members[index]; }
        }

        public void CopyTo(Array a, int index)
        {
            _members.CopyTo(a, index);
        }

        public int Count
        {
            get { return _members.Count; }
        }

        public bool IsSynchronized
        {
            get { return false; }
        }

        public object SyncRoot
        {
            get { return this; }
        }

        public IEnumerator GetEnumerator()
        {
            return _members.GetEnumerator();
        }

        public void Add(Member newMember)
        {
            _members.Add(newMember);
        }

        public void Remove(Member memberToRemove)
        {
            _members.Remove(memberToRemove);
        }
    }
}