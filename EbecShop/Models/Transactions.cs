﻿using System;
using System.Collections;

namespace EbecShop.Models
{
    public class Transactions : ICollection
    {
        private readonly ArrayList _transactions = new ArrayList();

        public Transaction this[int index]
        {
            get { return (Transaction) _transactions[index]; }
        }

        public void CopyTo(Array a, int index)
        {
            _transactions.CopyTo(a, index);
        }

        public int Count
        {
            get { return _transactions.Count; }
        }

        public bool IsSynchronized
        {
            get { return false; }
        }

        public object SyncRoot
        {
            get { return this; }
        }

        public IEnumerator GetEnumerator()
        {
            return _transactions.GetEnumerator();
        }

        public void Add(Transaction newTransaction)
        {
            _transactions.Add(newTransaction);
        }

        public void Remove(Transaction transactionToRemove)
        {
            _transactions.Remove(transactionToRemove);
        }
    }
}