﻿namespace EbecShop.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Scores { get; set; }
        public int CaptainId { get; set; }
    }
}