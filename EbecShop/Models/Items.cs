﻿using System;
using System.Collections;

namespace EbecShop.Models
{
    public class Items : ICollection
    {
        private readonly ArrayList _items = new ArrayList();

        public Item this[int index]
        {
            get { return (Item) _items[index]; }
        }

        public void CopyTo(Array a, int index)
        {
            _items.CopyTo(a, index);
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public bool IsSynchronized
        {
            get { return false; }
        }

        public object SyncRoot
        {
            get { return this; }
        }

        public IEnumerator GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        public void Add(Item newItem)
        {
            _items.Add(newItem);
        }

        public void Remove(Item itemToRemove)
        {
            _items.Remove(itemToRemove);
        }
    }
}