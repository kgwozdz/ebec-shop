﻿using System;

namespace EbecShop.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        public int ItemId { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
        public DateTime Data { get; set; }
    }
}