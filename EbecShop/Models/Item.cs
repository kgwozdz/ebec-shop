﻿namespace EbecShop.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string Code { get; set; }
        public int QuantityInWarehouse { get; set; }
        public int MaxQuantityInWarehouse { get; set; }
        public int MaxQuantityAvailablePerTeam { get; set; }
    }
}