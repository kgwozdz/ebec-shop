﻿using System;
using System.Collections;

namespace EbecShop.Models
{
    public class Teams : ICollection
    {
        private readonly ArrayList _teams = new ArrayList();

        public Team this[int index]
        {
            get { return (Team) _teams[index]; }
        }

        public void CopyTo(Array a, int index)
        {
            _teams.CopyTo(a, index);
        }

        public int Count
        {
            get { return _teams.Count; }
        }

        public bool IsSynchronized
        {
            get { return false; }
        }

        public object SyncRoot
        {
            get { return this; }
        }

        public IEnumerator GetEnumerator()
        {
            return _teams.GetEnumerator();
        }

        public void Add(Team newTeam)
        {
            _teams.Add(newTeam);
        }

        public void Remove(Team teamToRemove)
        {
            _teams.Remove(teamToRemove);
        }
    }
}